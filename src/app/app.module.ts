import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http'; // import untuk ambil data json
import { MyApp } from './app.component';

import { OptionsPage } from '../pages/options/options';
import { HomePage } from '../pages/home/home';
import { AddNewLogPage } from '../pages/add-new-log/add-new-log';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { CariLogPage } from '../pages/cari-log/cari-log';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { DataServiceProvider } from '../providers/data-service/data-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    OptionsPage,
    AddNewLogPage,
    LoginPage,
    CariLogPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule // import untuk ambil data json
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    OptionsPage,
    AddNewLogPage,
    LoginPage,
    CariLogPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    DataServiceProvider
  ]
})
export class AppModule {}
