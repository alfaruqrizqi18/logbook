import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CariLogPage } from './cari-log';

@NgModule({
  declarations: [
    CariLogPage,
  ],
  imports: [
    IonicPageModule.forChild(CariLogPage),
  ],
})
export class CariLogPageModule {}
