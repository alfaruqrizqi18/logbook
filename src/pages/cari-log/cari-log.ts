import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

/**
 * Generated class for the CariLogPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cari-log',
  templateUrl: 'cari-log.html',
})
export class CariLogPage {
data: any;
loading: any;
form = {nim: ''};
  constructor(
  	public navCtrl: NavController,
      public modalCtrl: ModalController,
      public loadingCtrl: LoadingController,
      public dataService: DataServiceProvider,
      public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CariLogPage');
  }

  doSubmit(){
   this.showLoader();
   this.dataService.searchListDataLogByNIM(this.form.nim).subscribe((result) => {
     this.loading.dismiss();
     this.data = result;
     console.log(result);
   })
 }

 showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Searching...'
    });

    this.loading.present();
  }

}
