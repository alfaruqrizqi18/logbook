import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
/**
 * Generated class for the OptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {
tabs : any;
  constructor(
  	public navCtrl: NavController,
  	public authService: AuthServiceProvider, 
  	public navParams: NavParams) {
    
    this.tabs = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OptionsPage');
  }

  doLogout(){
  	this.authService.logout().then((result) => {
      }, (err) => {
      });
      this.tabs.style.display = 'none';
      this.navCtrl.setRoot(LoginPage);
      window.location.reload(true);
  }

}
