import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

/**
 * Generated class for the AddNewLogPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-log',
  templateUrl: 'add-new-log.html',
})
export class AddNewLogPage {
tabs : any;
form = {log_content: ''}
data: any;
loading: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController,
    public dataService : DataServiceProvider,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController) {
  	this.tabs = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewDidLoad() {
    this.tabs.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabs.style.display = 'flex';
  }

  doSubmit(){
    if (this.form.log_content == "") {
      this.presentToast('Mohon isi semua form');
    }else{
      var data_post = {
        log_content : this.form.log_content,
        nim : localStorage.getItem('lb-nim')
      };
      this.showLoader();
      this.dataService.add_new_log(data_post).then((result) => {
      this.loading.dismiss();
      this.data = result;
      console.log(this.data);
      this.viewCtrl.dismiss();
      }, (err) => {
        this.presentToast("No data saved");
      this.loading.dismiss();
      });
    }
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Saving...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.present();
  }

}
