import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewLogPage } from './add-new-log';

@NgModule({
  declarations: [
    AddNewLogPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewLogPage),
  ],
})
export class AddNewLogPageModule {}
