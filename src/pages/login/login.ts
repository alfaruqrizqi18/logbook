import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import { CariLogPage } from '../../pages/cari-log/cari-log';


import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
loading: any;
form = { nim:'', password:'' };
data: any;
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams, 
  	public loadingCtrl: LoadingController,
  	public authService: AuthServiceProvider,
  	public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    var nim = localStorage.getItem('lb-nim');
    if (nim) {
      this.navCtrl.setRoot(TabsPage);
    }
  }

  toSearchPage(){
    this.navCtrl.push(CariLogPage);
  }

  doLogin() {
    if (this.form.nim == "" || this.form.password == "" ){
      this.presentToast('Mohon isi semua form');
    }else{
    	this.showLoader();
      this.authService.login(this.form).then((result) => {
      this.loading.dismiss();
        this.data = result;
        console.log(this.data);
        if (this.data.status == 'success') {
          localStorage.setItem('lb-nim', this.data.nim);
          localStorage.setItem('lb-username', this.data.username);
          localStorage.setItem('lb-owner', this.data.owner);
          this.navCtrl.setRoot(TabsPage);
        }else{
          this.presentToast("Username atau Password salah");
        }
      }, (err) => {
        this.presentToast("Username atau Password salah");
      this.loading.dismiss();
      });
    }
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticate'
    });

    this.loading.present();
  }
   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.present();
  }

}
