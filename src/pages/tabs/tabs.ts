import { Component } from '@angular/core';

import { OptionsPage } from '../options/options';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
tabs : any;
  tab1Root = HomePage;
  tab2Root = OptionsPage;

  constructor() {
  	this.tabs = document.querySelector('.tabbar.show-tabbar');
  }
  
}
