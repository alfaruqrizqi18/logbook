import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { AddNewLogPage } from '../../pages/add-new-log/add-new-log';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
tabs : any;
data: any;
loading: any;
  constructor(
      public navCtrl: NavController,
      public modalCtrl: ModalController,
      public loadingCtrl: LoadingController,
      public dataService: DataServiceProvider,
      public toastCtrl: ToastController) { 	
  }
  
  openAddModal() {
   let profileModal = this.modalCtrl.create(AddNewLogPage);
   profileModal.onDidDismiss(data => {
   this.getListDataLog();
   });
   profileModal.present();
 }

 ionViewDidEnter(){
    this.getListDataLog(); 
 }
 
 getListDataLog(){
   this.showLoader();
   this.dataService.getListDataLog().subscribe((result) => {
     this.loading.dismiss();
     this.data = result;
     console.log(result);
   })
 }

 showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Fetching data...'
    });

    this.loading.present();
  }

}
