import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DataServiceProvider {
public base_url;
  constructor(
  	public http: Http,
  	public authService: AuthServiceProvider) {
  	this.base_url = this.authService.base_url;
  }

  add_new_log(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'log/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }

  getListDataLog(){
  console.log(this.http.get(this.base_url+'log/?nim='+localStorage.getItem('lb-nim')).map(res => res.json()));
  return this.http.get(this.base_url+'log/?nim='+localStorage.getItem('lb-nim')).map(res => res.json());
  }

  searchListDataLogByNIM(nim){
  console.log(this.http.get(this.base_url+'log/?nim='+nim).map(res => res.json()));
  return this.http.get(this.base_url+'log/?nim='+nim).map(res => res.json());
  }

}
